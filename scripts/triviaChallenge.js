
window.onload = function() { getQuestions() };

var numQuestions, response, questions, isCorrect;
var cont = 0;
var results = [];

//Connect to the web service, get all questions and load first question for the user
async function getQuestions() {
    if (questions == null || questions == ""){
        questions = await fetch('https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean')
            .then((resp) => {
                return resp.json();
            }).then((resp) => {
                return resp.results;
            });
        numQuestions = questions.length;
        console.log(questions);
    }
    loadQuestion();
}

//Show the current question on the screen
function loadQuestion() {
    document.getElementById('title-category').innerHTML = questions[cont].category;
    document.getElementById('question').innerHTML = questions[cont].question;
    document.getElementById('num-question').innerHTML = (cont + 1) + " of " + numQuestions;
}

//When the user clicks an answer, validate if the answer is correct, 
//has reached the last question or continue with the next one
async function nextQuestion(answer) {
    
    isCorrect = questions[cont].correct_answer == answer;
    results.push([questions[cont].question, isCorrect]);

    if (cont == numQuestions - 1){
        localStorage.results = JSON.stringify(results);
        location.href = "Results.html";
    }
    
    cont++;

    loadQuestion();
}


