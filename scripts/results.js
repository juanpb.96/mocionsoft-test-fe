
window.onload = function () { showResults() };


function showResults() {

    var results = JSON.parse(localStorage.results); //Get results from the localStorage
    console.log(results);
    var parentDiv = document.getElementById("results-trivia");
    var questionDiv = document.getElementById("question-block");
    var score = 0;

    results.forEach(item => {
        //Create a div for the answer and description of the question
        var qQuestion = document.createElement("div"); 
        qQuestion.setAttribute("class","question");
        var qDescription = document.createElement("div");
        qDescription.setAttribute("class","questionDesc");
        var qAnswer = document.createElement("div");
        qAnswer.setAttribute("class","answer");
        var isCorrect = item[1] ? "✓" : "x"; //Choose whether use '+' or '-' if the answer was correct or not
        score = item[1] ? score + 1 : score; //Count how many answers were correct
        qDescription.innerHTML = item[0];  //Add description to the div
        qAnswer.innerHTML = isCorrect; //Add answer to the div
        //Add child elements to the question div
        qQuestion.appendChild(qDescription);
        qQuestion.appendChild(qAnswer);
        //Add a new question block to the parent div
        parentDiv.insertBefore(qQuestion, questionDiv);
    }); 

    document.getElementById("total-score").innerHTML = "You scored: " + score + "/10"; 
    parentDiv.removeChild(questionDiv); //Remove the div created on the html file
}

function playAgain() {
    localStorage.clear();
    location.href = "Index.html";
}